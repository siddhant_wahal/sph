Introduction
============

sph implements Smoothed Particle Hydrodynamics to solve the Fokker-Planck
equation 1D. 

The Fokker-Planck (FP) equation describes the evolution of the probability density
function (PDF) of the solution to a stochastic differential equation. The
FP equation is a convection-diffusion equation, which is solved here via
Smoothed Particle Hydrodynamics (SPH). In SPH, Lagrangian particles carry
physical properties of the flow - in this case the mass density and the probability
density. Formulating the problem using Lagrangian particles turns the PDE into a
system of ODEs. These ODEs are solved here via a 4th order Runge-Kutta scheme.
Flow properties at any given spatial location are obtained by
interpolating the particle properties via a regularizing kernel. For improved
accuracy, the bandwidth of the regularizing kernel can be adaptively varied by
requiring that each Lagrangian particle have a fixed number of nearest neighbors
in the simulation.

Requirements
===========

-  ``numpy``
-  ``sklearn 0.16`` or higher

Tests
=====

A few test cases where the analytical solution is available. 

- Ornstein-Uhlebeck process 
- Wiener process
