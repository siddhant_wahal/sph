import numpy as np
import sys
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
sys.path.append('../../')
from sph import *

###################################################################
#Compare SPH results against analytical solution of the FPK equation 
#for the Wiener process. 
###################################################################

def drift(x):
    dim = 1
    return np.zeros((dim))

def divDrift(x):
    dim = 1
    return np.zeros((dim))

def initPDF(x):
    dim = 1
    return multivariate_normal.pdf(x, mean=np.zeros(dim), cov=np.identity(dim))

dim = 1
krnl = M4(dim)

tFinal = 1.0

xMin = -10
xMax = 10

nParticlesList = [16, 32, 64, 128, 256]
numNbrs = 4

nQueryPts = 51
xQueryMin = -10
xQueryMax = 10
queryPts = np.linspace(xQueryMin, xQueryMax, nQueryPts)
sphPDF = np.empty((nQueryPts, len(nParticlesList)))
diffusionCoeff = 0.5

for i, n in enumerate(nParticlesList):

    xArr = np.linspace(xMin, xMax, n)
    coordArr = np.reshape(xArr, (1, n))
    massArr = 1.0 / n * np.ones((n))

    sph = SPHProblem(dim, n, \
                     numNbrs, krnl, \
                     drift, divDrift, \
                     diffusionCoeff, initPDF, adaptive=False)

    sph.initialize(coordArr, massArr)
    sph.run(tFinal)
    
    sphPDF[:, i]  = sph.getPDF(np.reshape(queryPts, (1, nQueryPts)))

    sph = None


plt.ioff()
plt.figure(1)
for i in range(len(nParticlesList)):
    plt.plot(queryPts, sphPDF[:, i], label='N = %d' % nParticlesList[i])
plt.plot(queryPts, \
         multivariate_normal.pdf(queryPts, mean=0, cov=(1.0 + tFinal)), \
         'ok', label='Analytical PDF')

plt.xlabel('$x$')
plt.ylabel('$p(x)$')
handles, labels = plt.gca().get_legend_handles_labels()
plt.gca().legend(handles, labels)
plt.savefig('numParticlesConvWiener.pdf')
plt.close()
