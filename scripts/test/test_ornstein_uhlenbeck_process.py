import numpy as np
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
import sys
sys.path.append('../../')
from sph import *

###################################################################
#Compare SPH results against analytical solution of the FPK equation 
#for the Ornstein-Uhlenbeck process. 
###################################################################

def drift(x, a):
    return  -1.0 * a * x

def divDrift(x, a):
    return -1.0 * a

def initPDF(x, sigma, a):
    dim = 1
    return multivariate_normal.pdf(x, mean=np.zeros(dim), \
            cov=np.identity(dim))
dim = 1
krnl = M4(dim)

tFinal = 2.0

xMin = -100.
xMax = 100.


nParticlesList = [16, 32, 64, 128, 256]
numNbrs = 8

nQueryPts = 51
xQueryMin = -10.0
xQueryMax = 10.0
queryPts = np.linspace(xQueryMin, xQueryMax, nQueryPts)
sphPDF = np.empty((nQueryPts, len(nParticlesList)))

sigma = 2.0
a = 1.0
diffusionCoeff = 0.5 * sigma * sigma
sphDivDrift = lambda x: divDrift(x, a)
sphDrift = lambda x: drift(x, a)
sphInitPDF = lambda x: initPDF(x, sigma, a)

for i, n in enumerate(nParticlesList):

    xArr = np.linspace(xMin, xMax, n)
    coordArr = np.reshape(xArr, (1, n))
    massArr = 1.0 / n * np.ones((n))

    sph = SPHProblem(dim, n, numNbrs, krnl, sphDrift, sphDivDrift, diffusionCoeff, \
            sphInitPDF)
    sph.initialize(coordArr, massArr)
    sph.run(tFinal)
    
    sphPDF[:, i]  = sph.getPDF(np.reshape(queryPts, (1, nQueryPts)))

plt.ioff()
plt.figure(0)
for i in range(len(nParticlesList)):
    plt.plot(queryPts, sphPDF[:, i], label='N = %d' % nParticlesList[i])

plt.plot(queryPts, \
         multivariate_normal.pdf(queryPts, mean=0, \
                                cov=(sigma * sigma / (2.0 * a))),\
        'ok', label='Analytical PDF')
plt.xlabel('$x$')
plt.ylabel('$p(x)$')
handles, labels = plt.gca().get_legend_handles_labels()
plt.gca().legend(handles, labels)
plt.savefig('numParticlesConvOUProcess.pdf')
plt.close()
