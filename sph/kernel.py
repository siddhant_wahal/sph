import abc
import numpy as np
from scipy.stats import multivariate_normal

class KernelBase(object):
    """Abstract base class for SPH kernels. A concrete implementation of an SPH
    kernel must supply the following methods
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def val(self, xi, xj, h):
        #Compute value of the kernel function centered at xj at some point
        #xi. h is the range of the kernel"""
        return

    @abc.abstractmethod
    def grad(self, xi, xj, h):
        #Compute gradient at xi of the kernel function of width h and centered
        #at xj"""
        return

class Lucy(KernelBase):
    """Lucy kernel from 2 to 4 dimensions
    Attributes:
    - dim: spatial dimension
    - alpha: constant 
    - compact: is the kernel function compactly supported?
    - support: extent of the support of the kernel function
    """


    def __init__(self, dim):
        self.dim = dim
        self.alpha = {2:(5.0/4.0) , 3:(5.0/np.pi) , 4:(105.0/16.0/np.pi) }
        self.compact = True
        self.support = 1.0
    
    def val(self, xi, xj, h):
        r = xi - xj
        R = np.sqrt(np.dot(r, r)) / h
        
        value = 0.0

        if R <= 1.0:
            value = ((self.alpha[self.dim] / (h ** self.dim)) 
                     * (1 + 3.0 * R) * (1.0 - R)**3.0)

        return value

    def grad(self, xi, xj, h):
        r = xi - xj
        R = np.sqrt(np.dot(r, r)) / h

        value = np.zeros(self.dim)


        if R < 1.0 and R > 0.0:
            value = (6.0 * self.alpha[self.dim]/ (h ** (self.dim + 2.0))
                    * (1 - R) ** 2.0 * (1 + R) * r / R)
        
        return value

class Gaussian(KernelBase):
    """d - dimensional Gaussian kernel
    Attributes:
    - dim: spatial dimension
    - compact: is the kernel function compactly supported?
    - support: extent of the support of the kernel function
    """


    def __init__(self, dim):
        self.dim = dim
        self.compact = False
        self.support = None
        self.eye = np.identity(self.dim)

    def val(self, xi, xj, h):

        return multivariate_normal.pdf(xi, mean=xj, 
                                           cov=(h * self.eye))

    def grad(self, xi, xj, h):

        cov = h * self.eye
        invCov = (1.0 / h) * self.eye

        grad = ((-invCov * (xi - xj)) 
                * multivariate_normal.pdf(xi, mean=xj, cov=cov)) 

        return np.reshape(grad, self.dim)


class M4(KernelBase):
    """M4 Kernel
    Attributes:
    - dim: spatial dimension
    - alpha: constant 
    - compact: is the kernel function compactly supported?
    - support: extent of the support of the kernel function
    """
    def __init__(self, dim):
        self.dim = dim
        self.compact = True
        self.support = 2.0
        self.alpha = {1:(1.0/6.0) , 2:(15.0 / (14.0 * np.pi)),
                      3:(1.0 / (4.0 * np.pi))}

    def val(self, xi, xj, h):
        r = xi - xj
        R = np.sqrt(np.dot(r, r)) / h
        
        value = 0.0

        if R <= 1.0:
            value = (2.0 - R) ** 3.0 - 4.0 * (1.0 - R) ** 3.0
        elif R <= 2.0:
            value = (2.0 - R) ** 3.0

        return self.alpha[self.dim] / (h ** self.dim) * value

    def grad(self, xi, xj, h):
        r = xi - xj
        norm_r = np.sqrt(np.dot(r, r))
        R = norm_r / h

        value = np.zeros(self.dim)


        if R > 0.0:
            if R < 1.0:
                value = ((-3.0 * (2.0 - R) ** 2.0 
                         + 12.0 * (1.0 - R) ** 2.0) * r / norm_r)
            elif R < 2.0:
                value = (-3.0 * (2.0 - R) ** 2.0) * r / norm_r
        
        return self.alpha[self.dim] / (h ** (self.dim + 1.0)) * value

    def hessian(self, xi, xj, h):
        r = xi - xj
        norm_r = np.sqrt(np.dot(r, r))
        R = norm_r / h

        value = np.zeros((self.dim, self.dim))
        if R > 0.0:
            mat1 = np.dot(r, r.T) / (norm_r * norm_r)
            mat2 = 1.0 / norm_r * (np.identity(self.dim) - mat1)

            if R < 1.0:
                value = (6.0 * (2.0 - R) - 24.0 * (1.0 - R)) \
                        / (h ** (self.dim + 2.0)) * mat1 \
                        + (-3.0 * (2.0 - R) ** 2.0 + 12.0 * (1.0 - R) ** 2.0) \
                        / (h ** (self.dim + 1.0)) * mat2
            elif R < 2.0:
                value = (6.0 * (2.0 - R)) / (h ** (self.dim + 2.0)) * mat1 \
                    - 3.0 * (2.0 - R) ** 2.0 / (h ** (self.dim + 1.0)) * mat2

        return self.alpha[self.dim] * value


