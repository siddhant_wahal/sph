import numpy as np
from particle import Particle
from kernel import *
import sys
from scipy.integrate import ode
import warnings

class SPHProblem(object):
    """Class that implements the solution of the Fokker-Planck-Kolmogorov
    equation using SPH

    Attributes:
    - dim: Spatial dimension
    - nParticles: Number of particles to use in the SPH simulation
    - numNbrs: Number of neighbors each particle must have. This is used to set
      the bandwidth of the SPH kernel
    - drift: The drift term in the FPK equation. This must be a callable object
      drift(x) where x is the spatial location
    - divDrift: The divergence of the drift term in the FPK equation. This must 
      be a callable object divDrift(x) where x is the spatial location
    - diffusion: Isotropic diffusion coefficient in the FPK equation
    - initPDF: The PDF at t=0, must be a callable object initPDF(x), where x is 
      the spatial location.
    - adaptive: Boolean that controls whether SPH kernel bandwidth is to be
      adaptively varied or not.
    - nComp: Number of components that must be integrated over time for each
      SPH particle. These will be the coordinate, the PDF value, and the mass
      density. 
    - particlesList: List of Particle objects in the simulation
    - integrator: ODE integrator


    Methods:
    - initialize: Initializes the SPH problem
    - pdfRHS: RHS of the particle PDF update equation
    - densityRHS: RHS of the particle density update equation
    - getPDF: Compute the PDF at given spatial locations
    - rhs: RHS of the time integrator
    - setIntegratorInitVal: Set the initial condition for the ODE integrator
    - reassignToParticles: Map integrator state at some time back to particle
      properties
    - callback: Callback function called at the end of each integrator time
      step
    - run: Run the SPH simulation
    """


    def __init__(self, dim, nParticles, \
                 numNbrs, kernel, \
                 drift, divDrift, diffusion, initPDF, \
                 adaptive=True):

        
        self.dim = dim
        self.nParticles = nParticles
        self.numNbrs = numNbrs
        self.kernel = kernel
        self.drift = drift
        self.divDrift = divDrift
        self.diffusion = diffusion
        self.initPDF = initPDF
        self.adaptive = adaptive
        self.nComp = self.dim + 2

        self.particlesList = None
        self.integrator = ode(self.rhs)
        
        self.integrator.set_integrator('dopri5')
        self.integrator.set_solout(self.callback)


    def initialize(self, coordArr, massArr):

        """Initialize SPH problem
        Input:
        - coordArr: Array of initial particle locations, must be dim x
        nParticles
        - massArr: Mass of the particles
        """

        #Initialize particlesList
        self.particlesList = [Particle(self.dim, coordArr[:, i], 
                                       massArr[i], self.numNbrs) 
                              for i in xrange(self.nParticles)]

        #Set the intial resolution and compute the interaction list for each
        #particle
        for particle in self.particlesList:
            particle.setInitialResolution(self.kernel, self.particlesList)
            particle.setInteractionList(self.kernel, self.particlesList, \
                    self.adaptive)

        #Set initial density
        for particle_i in self.particlesList:
            densitySum = 0.0
            for particle_j in particle_i.interactionList:

                val = 0.5 * (self.kernel.val(particle_i.coord,
                                         particle_j.coord,
                                         particle_i.h)
                            + self.kernel.val(particle_i.coord,
                                          particle_j.coord,
                                          particle_j.h))

                densitySum += particle_j.mass * val
            
            particle_i.density = densitySum


        #Set initial PDF
        for particle_i in self.particlesList:
            pdfSum = 0.0
            for particle_j in particle_i.interactionList:

                val = 0.5 * (self.kernel.val(particle_i.coord,
                                         particle_j.coord,
                                         particle_i.h)
                            + self.kernel.val(particle_i.coord,
                                          particle_j.coord,
                                          particle_j.h))

                pdfSum += ((particle_j.mass / particle_j.density)
                                  * self.initPDF(particle_j.coord) 
                                  * val)
            
            particle_i.pdf = pdfSum


            
    def pdfRHS(self, particle_i):

        """Compute RHS of the SPH PDF equation
        Input:
        - particle_i: Particle object at which to compute RHS
        """

        grad_vel = self.divDrift(particle_i.coord)
        laplacian = 0.0
        vel_i = self.drift(particle_i.coord)

        for particle_j in particle_i.interactionList:
            hess = 0.5 * (self.kernel.hessian(particle_i.coord,
                                           particle_j.coord,
                                           particle_i.h)
                         + self.kernel.hessian(particle_i.coord,
                                            particle_j.coord,
                                            particle_j.h))

            laplacian += (particle_j.mass / particle_j.density) \
                    * (particle_j.pdf - particle_i.pdf) * hess
                   
        return self.diffusion * laplacian - particle_i.pdf * grad_vel

    def densityRHS(self, particle_i):
        
        """Compute RHS of the SPH (mass) density equation
        Input:
        - particle_i: Particle object at which to compute RHS
        """

        return - particle_i.density * self.divDrift(particle_i.coord)

    
    def getPDF(self, xArr):
        """Interpolate PDF at query points in xArr from particle values
           Inputs:
           - xArr: Query locations, must be dim x nPts"""

        nPts = xArr.shape[1] 

        prob = np.zeros(nPts)

        for i in xrange(nPts):
            for particle in self.particlesList:
                k = self.kernel.val(xArr[:, i], particle.coord, particle.h)
                prob[i] += particle.mass / particle.density * particle.pdf * k
        
        return prob
    
    def rhs(self, t, y):
        """RHS of time integrator y' = rhs(t, y)
        Inputs:
        - t: time
        - y: vector of particle properties all stacked on top of each other
             y must have the following form:
                [particle_0.coord(0),
                 ...
                 particle_0.coord(dim - 1),
                 particle_0.density
                 particle_0.pdf,
                 particle_1.coord(0),
                 ...
                 particle_1.coord(dim - 1)
                 particle_1.density
                 particle_1.pdf,
                 ...]

        """

        self.reassignToParticles(y)

        rhsArr =  np.array([[self.drift(particle.coord), \
                             self.densityRHS(particle), \
                             self.pdfRHS(particle)] \
                             for particle in self.particlesList])
        return rhsArr.flatten()

    def setIntegratorInitVal(self):


        y = np.array([[particle.coord[:], particle.density, particle.pdf] \
                for particle in self.particlesList])
        
        self.integrator.set_initial_value(y.flatten())

    
    def reassignToParticles(self, y):
        """
        Map vector used for time integration back to particle properties
        Inputs:
        - y: The integrator state at some time t
        """

        y = np.reshape(y, [self.nParticles, self.nComp])

        for i in xrange(self.nParticles):
            self.particlesList[i].coord[:] = y[i, :self.dim]
            self.particlesList[i].density = y[i, -2]
            self.particlesList[i].pdf = y[i, -1]

    def callback(self, t, y):
        """Function to call at the end of every ode integrator time step
        Inputs:
        - t: time
        - y: Integrator state at some time t
        """

        #After every time step, set the interaction list for each particle
        for particle in self.particlesList:
            particle.setInteractionList(self.kernel, self.particlesList, \
                    self.adaptive)
        #And reassign the solution vector y to particle properties
        self.reassignToParticles(y)

    def run(self, tFinal):
        """
        Run the SPH simulation
        Inputs:
        - tFinal: Length of the time horizon for simulation
        """

        self.setIntegratorInitVal()
        sys.stdout.flush()

        dt = tFinal / 10.0

        while self.integrator.t < tFinal and self.integrator.successful():
            self.integrator.integrate(self.integrator.t + dt)
        
