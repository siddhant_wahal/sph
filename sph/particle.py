import numpy as np
from kernel import Lucy
from sklearn.neighbors import NearestNeighbors

class Particle(object):
    """Class that provides functionality for an SPH particle
    Attributes:
    - dim: Spatial dimension 
    - coord: Current spatial coordinate
    - mass: (fixed) Mass of the SPH particle
    - h: Range of this particle's influence. If the kernel is compactly
         supported, this particle will only interact with particles within 
         kernel.support * self.h distance. This attribute may change if a variable
         resolution SPH method is used.
    - velocity: Current velocity of the particle
    - density: Current (mass) density of the particle
    - pdf: Current pdf value of the particle
    - interactionList: List of other particles in the simulation that this
      particle interacts with

    Methods:
    - getInteractionList: Computes the interaction list of this particle
    """

    def __init__(self, dim, coord, mass, numNbrs):
        """
        Initialize particle with dim, coord and mass values
        The range of the particle's influence is set implicitly by specifying
        the number of nearest neighbors, numNbrs, it must have. 
        """
    
        self.dim = dim
        self.coord = coord.copy()
        self.mass = mass
        self.velocity = None
        self.density = None
        self.pdf = None


        self.radiusSearch = NearestNeighbors(algorithm='brute')
        self.nbrSearch = NearestNeighbors(n_neighbors=numNbrs, \
                algorithm='brute');
        
        self.interactionList = None

    def findRadiusNbrs(self, radius, particlesList):
        """
        Find neighbors within radius in particlesList
        Inputs:
        - particlesList: List of particle objects
        """
        
        coordArr = np.array([particle.coord for particle in particlesList])
        if coordArr.shape[1] == 1:
            self.radiusSearch.fit(coordArr.reshape(-1, 1))
            queryPt = self.coord.reshape(1, -1)
        else:
            self.radiusSearch.fit(coordArr)
            queryPt = self.coord

        indices = self.radiusSearch.radius_neighbors(queryPt, \
            radius=radius, return_distance=False)

        return list(indices[0])


    def findNearestNeighbors(self, particlesList):
        """
        Find numNbrs neighbors of the particle within particlesList
        Inputs:
        - particlesList: List of particle objects
        """

        coordArr = np.array([particle.coord for particle in particlesList])
        if coordArr.shape[1] == 1:
            self.nbrSearch.fit(coordArr.reshape(-1, 1))
            queryPt = self.coord.reshape(1, -1)
        else:
            self.nbrSearch.fit(coordArr)
            queryPt = self.coord

        distances, indices = self.nbrSearch.kneighbors(queryPt)

        return distances, list(indices[0])


    def setInitialResolution(self, kernel, particlesList):
        """
        Initialize the range of the particle's influence. The range is
        implicitly set by requiring the particle to possess at least numNbrs
        neighbors
        Inputs:
        - kernel: SPH interpolation kernel
        - particlesList: List of other particle objects in the simulation
        """
        
        distances, indices = self.findNearestNeighbors(particlesList)

        if kernel.compact:
            #if the kernel is compact, all nearest neighbors must be within 
            #self.h * kernel.support distance 
            self.h = np.amax(distances) / kernel.support
        else: 
            #If the kernel isn't compact, use this as a length scale
            self.h = np.amax(distances)


    def setInteractionList(self, kernel, particlesList, adaptive):
        """
        Returns the interaction list of this particle from all particles
        in particlesList. 

        Inputs:
        - kernel: Kernel employed in the SPH approximation
        - particlesList: list of all particles in the SPH simulation. This
          should be a list of Particle objects.
        - adaptive: Boolean that specifies whether self.h is allowed to vary or
          whether it stays constant at its initial value. If adaptive is True,
          self.h varies to keep the number of neighbors of the particle
          constant.
        """
        
        if adaptive:
            distances, indices = self.findNearestNeighbors(particlesList)
        else:
            indices = self.findRadiusNbrs(kernel.support * self.h, particlesList)


        if kernel.compact:
            if adaptive:
                self.h = np.amax(distances) / kernel.support
            
            self.interactionList = [particlesList[idx] for idx in indices]
        else: 
            if adaptive:
                self.h = np.amax(distances)
            self.interactionList = particlesList
